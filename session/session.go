package session

import (
	"crypto/rand"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"

	"bitbucket.org/trackit/models"

	"github.com/gorilla/sessions"
)

type uSess struct {
	uid       int64
	createdAt time.Time
	lifeTime  time.Duration
}

var sess struct {
	sync.RWMutex
	safe map[string]uSess
}

var store = sessions.NewCookieStore([]byte("something-very-secret"))

func init() {
	sess.safe = make(map[string]uSess)

	ticker := time.NewTicker(1 * time.Hour)
	quit := make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				timeNow := time.Now()
				sess.Lock()
				for k, v := range sess.safe {
					lt := v.createdAt.Add(v.lifeTime)
					if lt.Unix() < timeNow.Unix() {
						delete(sess.safe, k)
					}
				}
				sess.Unlock()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}

func genToken() (string, error) {
	key := make([]byte, 32)

	_, err := rand.Read(key)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", key), nil
}

// InitSession ...
func InitSession(w http.ResponseWriter, r *http.Request) (user *models.User, err error) {
	session, err := store.Get(r, "session")
	if err != nil {
		return
	}

	s := session.Values["session_id"]
	if s == nil {
		return user, errors.New("session is nil")
	}
	sess.RLock()
	_, ok := sess.safe[s.(string)]
	sess.RUnlock()
	if !ok {
		return user, errors.New("session is not found")
	}
	sess.RLock()
	user, err = models.GetUserByID(sess.safe[s.(string)].uid)
	sess.RUnlock()
	if err != nil {
		return
	}
	sess.Lock()
	sess.safe[s.(string)] = uSess{uid: sess.safe[s.(string)].uid, createdAt: time.Now(), lifeTime: sess.safe[s.(string)].lifeTime}
	sess.Unlock()
	return
}

// DeleteSession ...
func DeleteSession(w http.ResponseWriter, r *http.Request) {
	session, err := store.Get(r, "session")
	if err != nil {
		return
	}
	s := session.Values["session_id"]
	sess.Lock()
	delete(sess.safe, s.(string))
	sess.Unlock()
}

// CreateSession ...
func CreateSession(w http.ResponseWriter, r *http.Request, id int64, remember bool) error {
	var maxAge int
	if remember {
		maxAge = 30 * 24 * 3600 // 1 mounth
	} else {
		maxAge = 0
	}

	session, err := store.New(r, "session")
	if err != nil {
		return err
	}

	token, err := genToken()
	if err != nil {
		return err
	}

	session.Options = &sessions.Options{MaxAge: maxAge}
	session.Values["session_id"] = token

	err = session.Save(r, w)
	if err != nil {
		return err
	}
	lt := time.Hour
	if maxAge != 0 {
		lt = 30 * 24 * time.Hour
	}
	sess.Lock()
	sess.safe[token] = uSess{uid: id, createdAt: time.Now(), lifeTime: lt}
	sess.Unlock()

	return nil
}
