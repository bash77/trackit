package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"

	"github.com/gorilla/context"

	"bitbucket.org/trackit/handlers"
	"bitbucket.org/trackit/models"
	"bitbucket.org/trackit/session"
	"github.com/go-zoo/bone"
	"github.com/gorilla/csrf"
	"github.com/justinas/alice"
	yaml "gopkg.in/yaml.v2"
)

var logFile *os.File

type settings struct {
	DB struct {
		URL string `yaml:"url"`
	}
}

// Conf ...
var Conf settings

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	logFile, err := os.OpenFile("trackit.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	log.SetOutput(logFile)

	appDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	configFile, err := ioutil.ReadFile(appDir + "/config.yml")
	if err != nil {
		log.Fatal(err)
	}

	err = yaml.Unmarshal(configFile, &Conf)
	if err != nil {
		log.Fatal(err)
	}
	err = models.InitDatabase(Conf.DB.URL)
	if err != nil {
		log.Fatal(err)
	}

	err = handlers.InitTemplates(appDir)
	if err != nil {
		log.Fatal(err)
	}
}

func userAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, err := session.InitSession(w, r)
		if err != nil && r.URL.Path != "/" {
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}
		context.Set(r, "user", u)
		h.ServeHTTP(w, r)
	})
}

func main() {
	defer logFile.Close()
	mux := bone.New()
	mw := alice.New(context.ClearHandler, userAuth)

	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	mux.Get("/dashboard", mw.ThenFunc(handlers.DashBoardHandler))

	mux.Get("/issues", mw.ThenFunc(handlers.IssuesHandler))
	mux.Get("/issues/new", mw.ThenFunc(handlers.IssuesNewHandler))
	mux.Get("/issues/:id", mw.ThenFunc(handlers.IssuesIDHandler))

	mux.GetFunc("/", handlers.IndexHandler)
	mux.PostFunc("/login", handlers.LoginHandler)
	mux.GetFunc("/logout", handlers.LogoutHandler)
	mux.HandleFunc("/registration", handlers.RegistrationHandler)

	CSRF := csrf.Protect([]byte("32-byte-long-auth-key"))
	fmt.Println("Server started on port 8080 ...")
	http.ListenAndServeTLS(":8080", "server.crt", "server.key", CSRF(mux))
}
