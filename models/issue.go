package models

import "time"

// Issue ...
type Issue struct {
	ID          int64     `xorm:"'id' pk autoincr"`
	OwnerID     int64     `xorm:"'owner_id'"`
	CreatedAt   time.Time `xorm:"'created_at'"`
	FinishAt    time.Time `xorm:"'finish_at'"`
	Title       string    `xorm:"'title'"`
	Body        string    `xorm:"'body'"`
	TimeControl time.Time `xorm:"'time_control'"`
	ExecutorID  int64     `xorm:"'executor_id'"`
	StatusID    int64     `xorm:"'status_id'"`
}

// Status ...
type Status struct {
	ID     int64  `xorm:"'id' pk autoincr"`
	Name   string `xorm:"'name' varchar(25)"`
	Closed bool   `xorm:"'closed'"`
}

// Comment ...
type Comment struct {
	ID      int64  `xorm:"'id' pk autoincr"`
	IssueID int64  `xorm:"'issue_id'"`
	Text    string `xorm:"'text'"`
}
