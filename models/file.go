package models

import (
	"crypto/md5"
	"fmt"
	"strings"
	"time"
)

const (
	// IssueType ...
	IssueType = 0
	// CommentType ...
	CommentType = 1
)

// File ...
type File struct {
	ID         int64     `xorm:"'id' pk autoincr"`
	ParentType int       `xorm:"'parent_type'"`
	ParentID   int64     `xorm:"'parent_id'"`
	Name       string    `xorm:"'name' varchar(50)"`
	Hash       string    `xorm:"'hash' varchar(50)"`
	Body       *[]byte   `xorm:"'body'"`
	CreatedAt  time.Time `xorm:"'created_at'"`
}

// CreateFile ...
func CreateFile(body *[]byte, fn string) string {
	hash := fmt.Sprintf("%x", md5.Sum(*body))
	fname := strings.Split(fn, ".")
	p := fmt.Sprintf("./files/%s/%s/%s.%s", hash[0:2], hash[2:4], hash[4:], fname[len(fname)-1])
	return p
}
