package models

import (
	"log"

	"github.com/go-xorm/xorm"

	// postgres
	_ "github.com/lib/pq"
)

var orm *xorm.Engine

// InitDatabase ...
func InitDatabase(url string) (err error) {
	log.Println("Database initialization: Start ")
	defer log.Println("Database initialization: Complete")
	orm, err = xorm.NewEngine("postgres", url)
	if err != nil {
		return
	}

	err = orm.CreateTables(&User{}, &Role{}, Issue{}, Comment{}, Status{}, File{})
	if err != nil {
		return
	}

	_, err = orm.Insert(&Role{ID: 1, Name: "DefaulUser", IsIssueCreate: true})
	if err != nil {
		log.Println(err)
	}
	return nil
}

func sessionRelease(s *xorm.Session) {
	if !s.IsCommitedOrRollbacked {
		s.Rollback()
	}
	s.Close()
}
