package models

// Role ...
type Role struct {
	ID            int64  `xorm:"'id' pk autoincr"`
	Name          string `xorm:"'name' varchar(30) notnull unique"`
	IsUserCreate  bool   `xorm:"'is_user_create' notnull default false"`
	IsUserEdit    bool   `xorm:"'is_user_edit' notnull default false"`
	IsUserBlock   bool   `xorm:"'is_user_block' notnull default false"`
	IsUserDelete  bool   `xorm:"'is_user_delete' notnull default false"`
	IsIssueCreate bool   `xorm:"'is_issue_create' notnull default true"`
	IsIssueDelete bool   `xorm:"'is_issue_delete' notnull default false"`
}
