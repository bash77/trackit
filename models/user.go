package models

import (
	"crypto/md5"
	"crypto/sha1"
	"errors"
	"fmt"
	"io"
	"reflect"
	"strings"
	"time"
)

// User ...
type User struct {
	ID         int64     `xorm:"'id' pk autoincr"`
	Username   string    `xorm:"'username' varchar(25) notnull"`
	LowerName  string    `xorm:"'lowername' varchar(25) unique notnull"`
	Password   string    `xorm:"'password' notnull"`
	Email      string    `xorm:"'email' varchar(25) unique notnull"`
	FirstName  string    `xorm:"'first_name' varchar(25) notnull"`
	MiddleName string    `xorm:"'middle_name' varchar(25)"`
	LastName   string    `xorm:"'last_name' varchar(25) notnull"`
	IsActive   bool      `xorm:"'is_active' notnull default true"`
	CreatedAt  time.Time `xorm:"'created_at'"`
	RoleID     int64     `xorm:"'role_id' notnull"`
}

// EncodePasswd ...
func (u *User) EncodePasswd() {
	sh := sha1.New()
	io.WriteString(sh, u.Password)

	md := md5.New()
	io.WriteString(md, u.Password)

	u.Password = fmt.Sprintf("%x:%x", sh.Sum(nil), md.Sum(nil))
}

// ValidatePassword ...
func (u *User) ValidatePassword(passwd string) bool {
	newUser := &User{Password: passwd}
	newUser.EncodePasswd()
	return u.Password == newUser.Password
}

// GetUserByID ...
func GetUserByID(id int64) (*User, error) {
	u := new(User)
	has, err := orm.Id(id).Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, errors.New("User not found")
	}
	return u, nil
}

// GetUserByName ...
func GetUserByName(name string) (*User, error) {
	if len(name) == 0 {
		return nil, errors.New("User not found")
	}
	u := &User{LowerName: strings.ToLower(name)}
	has, err := orm.Get(u)
	if err != nil {
		return nil, err
	} else if !has {
		return nil, errors.New("User not found")
	}
	return u, nil
}

// GetUserAccess ...
func (u *User) GetUserAccess(actionName string) bool {
	r := &Role{ID: u.RoleID}
	has, err := orm.Get(r)
	if err != nil || !has {
		return false
	}
	v := reflect.ValueOf(r).Elem().FieldByName(actionName)

	return v.Bool()
}

// IsUserExist ...
func IsUserExist(uid int64, name string) (bool, error) {
	if len(name) == 0 {
		return false, nil
	}
	return orm.Where("id!=?", uid).Get(&User{LowerName: strings.ToLower(name)})
}

// IsEmailExist ...
func IsEmailExist(uid int64, email string) (bool, error) {
	if len(email) == 0 {
		return false, nil
	}
	return orm.Where("email!=?", uid).Get(&User{Email: email})
}

// CreateUser ...
func CreateUser(u *User) (err error) {
	isExist, err := IsUserExist(0, u.Username)
	if err != nil {
		return err
	} else if isExist {
		return errors.New("User already exist")
	}

	u.Email = strings.ToLower(u.Email)
	isExist, err = IsEmailExist(0, u.Email)
	if err != nil {
		return err
	} else if isExist {
		return errors.New("Email already exist")
	}

	u.LowerName = strings.ToLower(u.Username)
	u.EncodePasswd()
	u.CreatedAt = time.Now()

	s := orm.NewSession()
	defer sessionRelease(s)
	if err = s.Begin(); err != nil {
		return err
	}

	if _, err = s.Insert(u); err != nil {
		return err
	}

	return s.Commit()
}
