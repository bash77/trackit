package handlers

import (
	"net/http"

	"bitbucket.org/trackit/models"
	"github.com/gorilla/csrf"
)

// RegistrationHandler ...
func RegistrationHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		wd := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r), "lang": "en"}
		renderTemplate(w, "registration.html", &wd)
		return
	} else if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		u := new(models.User)
		u.Username = r.PostFormValue("username")
		u.Password = r.PostFormValue("password")
		u.Email = r.PostFormValue("email")
		u.FirstName = r.PostFormValue("firstname")
		u.MiddleName = r.PostFormValue("middlename")
		u.LastName = r.PostFormValue("lastname")
		u.RoleID = 1

		err = models.CreateUser(u)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		wd := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r), "lang": "en"}
		renderTemplate(w, "index.html", &wd)
	} else {
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	}
}
