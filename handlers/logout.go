package handlers

import (
	"net/http"

	"bitbucket.org/trackit/session"
)

// LogoutHandler ...
func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	session.DeleteSession(w, r)
	http.Redirect(w, r, "/dashboard", http.StatusSeeOther)
}
