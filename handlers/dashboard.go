package handlers

import (
	"net/http"

	"bitbucket.org/trackit/models"
	"github.com/gorilla/context"
	"github.com/gorilla/csrf"
)

// DashBoardHandler ...
func DashBoardHandler(w http.ResponseWriter, r *http.Request) {
	user := context.Get(r, "user").(*models.User)
	wd := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r),
		"lang":       "en",
		"id":         user.ID,
		"first_name": user.FirstName}
	renderTemplate(w, "dashboard.html", &wd)
	// renderTemplate(w, "index.tmpl", nil)
	// } else {
	// http.Error(w, "Access Forbidden for you role", http.StatusForbidden)
	// }
}
