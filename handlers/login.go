package handlers

import (
	"net/http"

	"bitbucket.org/trackit/models"
	"bitbucket.org/trackit/session"
)

// LoginHandler ...
func LoginHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	remember := false
	if r.FormValue("remember") == "on" {
		remember = true
	}

	user, err := models.GetUserByName(r.FormValue("username"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if !user.ValidatePassword(r.FormValue("password")) {
		http.Error(w, "Invalid password", http.StatusInternalServerError)
		return
	}

	err = session.CreateSession(w, r, user.ID, remember)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	http.Redirect(w, r, "/dashboard", http.StatusSeeOther)
}
