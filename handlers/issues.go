package handlers

import (
	"net/http"

	"github.com/gorilla/csrf"
)

// IssuesHandler ...
func IssuesHandler(w http.ResponseWriter, r *http.Request) {
	wd := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r), "lang": "en"}
	renderTemplate(w, "tickets.html", &wd)
}

// IssuesNewHandler ...
func IssuesNewHandler(w http.ResponseWriter, r *http.Request) {

}

// IssuesIDHandler ...
func IssuesIDHandler(w http.ResponseWriter, r *http.Request) {

}
