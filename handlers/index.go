package handlers

import (
	"net/http"

	"bitbucket.org/trackit/session"
	"github.com/gorilla/csrf"
)

// IndexHandler ...
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	_, err := session.InitSession(w, r)
	if err != nil {
		wd := map[string]interface{}{csrf.TemplateTag: csrf.TemplateField(r), "lang": "en"}
		renderTemplate(w, "index.html", &wd)
		return
	}
	http.Redirect(w, r, "/dashboard", http.StatusSeeOther)
}
